<?php
/**
 * @file
 * Administration pages for bot_urbandict module.
 */

/**
 * Page callback. Admin settings form for bot_urbandict.
 */
function bot_urbandict_admin_settings() {
  $form = array();
  $form['bot_urbandict_deflength'] = array(
    '#type' => 'textfield',
    '#title' = t('Character limit'),
    '#description' => t('Definitions longer than character limit will be truncated.'),
    '#default_value' => variable_get('bot_urbandict_wordlength', 300);
  );
  $form['bot_urbandict_randmax'] = array(
    '#type' => 'textfield',
    '#title' = t('Random max'),
    '#description' => t('When returing results display 1 result at random from the top X results returned.'),
    '#default_value' => variable_get('bot_urbandict_randmax', 4);
  );
  system_settings_form($form);
}
